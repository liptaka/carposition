function storeCarPosition() {
	window.localStorage.setItem("carPositionLati", actualPos.lati); 						
	window.localStorage.setItem("carPositionLongi", actualPos.longi);

}

function LoadPosition() {
	var latitude = parseFloat(window.localStorage.getItem("carPositionLati"));
	var longitude = parseFloat(window.localStorage.getItem("carPositionLongi"));
	posCar = {
		lat : latitude,
		lng : longitude
	}; //global

}

function geoFindMe() {
	var output = document.getElementById("map");

	if (!navigator.geolocation) {
		output.innerHTML = "<p>Geolocation is not supported by your browser</p>";
		return;
	}

	function success(position) {
		var latitude = position.coords.latitude;
		var longitude = position.coords.longitude;
		console.log("Position " + position);
		console.log("Latitude " + latitude);
		console.log("Longtitude " + longitude);
		actualPos = {
			lati : latitude,
			longi : longitude
		}; // global
		initMap(latitude, longitude);
	}

	function error() {
		output.innerHTML = "Unable to retrieve your location";
	}

	navigator.geolocation.getCurrentPosition(success, error);
}

function initMap(a, b) {
	var pos = {
		lat : a,
		lng : b
	};
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom : 10,
		center : pos
	});
	createMarker(pos);

	if (typeof (posCar) != 'undefined') {// examine whether there
		createMarker(posCar);
	}

	function createMarker(posi) {
		var marker = new google.maps.Marker({
			map : map,
			position : posi
		});
	}

	console.log(pos.lat + " " + pos.lng);
}
